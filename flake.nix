{
  description = "NixOS Flake";

  inputs = {
    nixpkgs.url = "github:NixOs/nixpkgs/nixos-unstable
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    lanzaboote.url = "github:nix-community/lanzaboote/v0.3.0";
    lanzaboote.inputs.nixpkgs.follows = "nixpkgs";
    impermanence.url = "github:nix-community/impermanence";
    nix-gaming.url = "github:fufexan/nix-gaming";
    nix-citizen.url = "github:LovingMelody/nix-citizen";
    nix-citizen.inputs.nix-gaming.follows = "nix-gaming";
  };

  outputs = { nixpkgs, home-manager, lanzaboote, impermanence, nix-gaming, nix-citizen, ... }@inputs: {
    nixosConfigurations = {
      System-007 = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = { inherit inputs; };
        modules = [
          ./configuration.nix
          impermanence.nixosModules.impermanence
          home-manager.nixosModules.home-manager {
            home-manager.useGlobalPkgs = true
            home-manager.useUserPackages = true
            home-manager.users.amethyst = import ./home.nix;
          }
          lanzaboote.nixosModules.lanzaboote
          boot.lanzaboote = {
            enable = true;
            pkiBundle = "/etc/secureboot";
          };
        ]
      }
    }
  }
}