{ pkgs, ... }: {
  home = {
    username = "amethyst";
    homeDirectory = "/home/amethyst";
  };

  home.packages = with pkgs; [
    tree
    inputs.nix-citizen.packages.${system}.lug-helper
    lutris
    steam
    prismlauncher
    gzdoom
    bless
    libreoffice-qt-fresh
    krita
    gimp-with-plugins
    thunderbird
    links2
    putty
    virt-viewer
    opensnitch-ui
    audacious
    audacious-plugins
    starship
    oh-my-zsh
    easyeffects
    vlc
    krename
    lshw-gui
    psensor
    nvtopPackages.full
    jdk17
    jdk
    virt-manager
    podman
    podman-desktop
    podman-compose
    gnome.gnome-calculator
    sqlitebrowser
    openrgb
    tailscale
    cloudflare-warp
    antimicrox
    gamescope
    gamemode
    git-credential-oauth
    hwinfo
    i2c-tools
    libappindicator
    mat2
    soundconverter
    spicetify-cli
    unar
    yt-dlp
    kdePackages = {
      qtstyleplugin-kvantum;
      ark;
      kgpg;
      okular;
      kleopatra;
      kde-gtk-config;
      kdeplasma-addons;
      spectacle;
    }
  ];

  xdg = {
    configFile = {
      "kitty/themesUltraViolent.conf".source = pkgs.fetchurl {
        url = "https://gitlab.com/Amethyst_Nightshade/nixos-config/-/raw/main/UltraViolent.conf?ref_type=heads&inline=false";
        hash = "sha512-jpxTs7Wr4zij95DKfrKmk+CwSgY2TdWFeVymSyP2LWioq4e/qsXoT15k91v8ju2msyHZ3yXzH15Kgc+JHllSOg==";
      }
    }
  }

  programs = {
    home-manager.enable = true;
    git.enable = true;

    kitty = {

      enable = true;
      shellIntegration.enableZshIntegration = true;
      theme = "UltraViolent";
    };
    starship = {
      enable = true;
      enableZshIntegration = true;
      settings = {
      "$schema" = 'https://starship.rs/config-schema.json';

      format = lib.concatStrings [
      "$username"
      "$hostname"
      "$directory"
      "$cmd_duration"
      "$line_break"
      "$character"
      ];

      username = {
      format = '[$user]($style):';
      style_user = 'bold purple';
      show_always = true;
      };

      hostname = {
        ssh_only = false;
        format = '[$hostname]($style) ';
        style = "bold blue";
      };

      directory = {
        style = "blue";
      };

        character = {
          success_symbol = "[❯](purple)";
          error_symbol = "[❯](red)";
        };
      };
    };
    zsh = {
      enable = true;
      enableCompletion = true;
      shellAliases = {
        icat="kitty +kitten icat"
        kitty-ssh="kitty +kitten ssh"
        kitty-diff="kitty +kitten diff"
        vscli="codium --unity-launch $1"
      };
      oh-my-zsh {
        enable = true;
        theme = "none";
        plugins = [
          "starship"
          "git"
          "extract"
          "safe-paste"
          "sudo"
          "systemadmin"
        ];
      };
      initExtra = ''
        # Wayland config. KEEP TOWARDS BOTTOM
        if [ "$XDG_SESSION_TYPE" = "wayland" ]; then
          export MOZ_ENABLE_WAYLAND=1
          alias vscli="codium --enable-features=UseOzonePlatform,WaylandWindowDecorations --ozone-platform=wayland --unity-launch $1"
        fi
      '';
    };



  };

  services {
    opensnitch-ui.enable = true;
  };


  # Nicely reload system units when changing configs
  systemd.user.startServices = "sd-switch";

  # DO NOT TOUCH
  home.stateVersion = "24.05"; # DID YOU READ THE COMMENT?
}