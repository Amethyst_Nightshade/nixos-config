# NixOS configuration

{ config, lib, pkgs, ...}:

{
  imports =
    [
      ./hardware-configuration.nix
      inputs.nix-gaming.nixosModules.pipewireLowLatency
      inputs.nix-gaming.nixosModules.platformOptimizations
    ];

  boot.loader.systemd-boot.enable = lib.mkForce false;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "System-007"
  networking.networkmanager.enable = true;

  powerManagement.enable = true;

  security = {
    rtkit.enable = true;
    polkit.enable = true;
    apparmor.enable = true;
    sudo-rs.enable = true;
    sudo-rs.execWheelOnly = true;
  };

  environment.memoryAllocator.provider = "graphene-hardened-light";

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  nixpkgs.config.allowUnfree = true;

  # NOVIDEO

  # Enable OpenGL
  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
  };

  services.xserver.videoDrivers = ["nvidia"];

hardware.nvidia = {

    # Modesetting is required.
    modesetting.enable = true;

    # Nvidia power management. Experimental, and can cause sleep/suspend to fail.
    # Enable this if you have graphical corruption issues or application crashes after waking
    # up from sleep. This fixes it by saving the entire VRAM memory to /tmp/ instead 
    # of just the bare essentials.
    powerManagement.enable = false;

    # Fine-grained power management. Turns off GPU when not in use.
    # Experimental and only works on modern Nvidia GPUs (Turing or newer).
    powerManagement.finegrained = false;

    # Use the NVidia open source kernel module (not to be confused with the
    # independent third-party "nouveau" open source driver).
    # Support is limited to the Turing and later architectures. Full list of 
    # supported GPUs is at: 
    # https://github.com/NVIDIA/open-gpu-kernel-modules#compatible-gpus 
    # Only available from driver 515.43.04+
    # Currently alpha-quality/buggy, so false is currently the recommended setting.
    open = false;

    # Enable the Nvidia settings menu,
	  # accessible via `nvidia-settings`.
    nvidiaSettings = true;

    # Optionally, you may need to select the appropriate driver version for your specific GPU.
    package = config.boot.kernelPackages.nvidiaPackages.stable;
  };


  time.timeZone = "America/New_York"

  i18n.defaultLocale = "en_US.UTF-8";

  services.xserver.enable = true;
  services.displayManager.sddm.enable = true;
  services.displayManager.sddm.wayland.enable = true;
  services.desktopManager.plasma6.enable = true;
  services.displayManager.defaultSession = "plasma";
  programs.xwayland.enable = true;

  environment.plasma6.excludePackages = with pkgs.kdePackages; [
    plasma-browser-integration
    konsole
    oxygen
    elisa
    kate
    khelpcenter
  ];


  services = {
    libinput.enable = true;
    xserver.xkb.layout = "us";
    printing.enable = true;
    samba.enable = true;
    flatpak.enable = true;
    gvfs.enable = true;

    usbguard = {
      enable = true;
      usbguard.IPCAllowedGroups = [ wheel ];
    };

    gamemode = {
      enable = true;
      capSysNice = true;
      enableRenice = true;
    };

    zram-generator = {
      enable = true;
      settings = {
        zram0 {
        zram-size = ram
        compression-algorithm = zstd
        swap-priority = 100
        fs-type = swap
        writeback-device=/dev/disk/by-id/
       }
      };
    };

    chrony = {
      enable = true;
      servers = [
        "0.nixos.pool.ntp.org"
        "1.nixos.pool.ntp.org"
        "2.nixos.pool.ntp.org"
        "3.nixos.pool.ntp.org"
      ];
      extraConfig = ''
        server time.cloudflare.com iburst nts
        maxupdateskew 100
        minsources 3
      '';
    };

    tailscale = {
      enable = true;
      openFirewall = true;
      useRoutingFeatures = "client";
    };

    pipewire = {
      enable = true;
      pulse.enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;

      lowLatency = {
        # enable this module
        enable = true;
        # defaults (no need to be set unless modified)
        quantum = 64;
        rate = 48000;
      };
    };
  };

  users.defaultUserShell = pkgs.bashInteractive;

  users.users.amethyst = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    shell = pkgs.zsh;
    linger = true;
    initialHashedPassword = "";
  };


  environment.systemPackages = with pkgs; [
    linuxKernel.kernels.linux_xanmod_latest
    linuxKernel.packages.linux_xanmod_latest.opensnitch-ebpf
    linuxKernel.packages.linux_xanmod_latest.lkrg
    opensnitch
    microcodeAmd
    sbctl
    efitools
    efibootmgr
    lvm2
    zram-generator
    cryptsetup
    usbguard
    openssh
    curlFull
    traceroute
    inetutils
    nano
    git
    zsh
    bashInteractive
    kitty
    librewolf
    brave
    vscodium-fhs
    btop
    htop
    hyfetch
  ];

  environment.variables.EDITOR = "nano";
  environment.variables.SUDO_EDITOR = "rnano";

  programs.zsh.enable = true;
  programs.steam.platformOptimizations.enable = true;


  # DO NOT TOUCH
  system.stateVersion = "24.05"; # Did you read the comment?
}